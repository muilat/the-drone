# Introduction

There is a major new technology that is destined to be a disruptive force in the field of transportation: **the drone**.
Just as the mobile phone allowed developing countries to leapfrog older technologies for personal communication, the
drone has the potential to leapfrog traditional transportation infrastructure.

Useful drone functions include delivery of small items that are (urgently) needed in locations with difficult access.

# Assumptions

### Delivery frequency

A drone can go out for delivery more than one time in a day, every delivery will have a session that is used to identify
the delivery and every item delivered in that session.

### LOADED state

When a drone is loaded with enough items, an endpoint will be called to change the drone state to LOADED. This is the
same for DELIVERING, DELIVERED AND RETURNING. When a drone is put in IDLE state, it's activeSession is cleared (set to
null) and when it goes back to loading; a new session is created

### Battery capacity

There would be an endpoint to frequently update the drone battery capacity, a pipeline will be ideal in real-life
scenario

# Running the App

## Compile the project

In the root directory, run mvn clean package

## Non-docker mode

cd target/

java -jar drone-0.0.1-SNAPSHOT.jar

## Docker mode

Ensure you have docker installed -> https://docs.docker.com/get-docker/

run

docker build -f DockerFile -t drone-app .

docker run -t -p 8080:8080 drone-app

**The app will run on http://localhost:8080**

## Swagger-ui

http://localhost:8080/drone/swagger-ui/

# Improvements

- Cache drone with list of medications and clear the cache when the drone goes to RETURNING/IDLE state
- Store drone sessions in a separate table so the items loaded into a drone at a particular point/session can be
  retried.
- save audit/log in database
- configuration notification/alert when the scheduler reads a drone with battery level below 25%