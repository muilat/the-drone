package com.musala.drone.mapper;

import com.musala.drone.domain.data.drone.Drone;
import com.musala.drone.domain.data.medication.Medication;
import com.musala.drone.domain.dto.drone.DroneCreateInput;
import com.musala.drone.domain.dto.medication.MedicationCreateInput;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface DroneMapper {
    @Mapping(target = "state", ignore = true)
    @Mapping(target = "createdDate", ignore = true)
    @Mapping(target = "lastModifiedDate", ignore = true)
    @Mapping(target = "activeLoadingSession", ignore = true)
    Drone droneCreateInputToDrone(DroneCreateInput input);

    @Mapping(target = "createdDate", ignore = true)
    @Mapping(target = "lastModifiedDate", ignore = true)
    @Mapping(target = "loadingSession", ignore = true)
    Medication medicationCreateInputToMedication(MedicationCreateInput input);
}
