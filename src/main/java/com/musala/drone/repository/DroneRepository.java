package com.musala.drone.repository;

import com.musala.drone.domain.data.drone.Drone;
import com.musala.drone.domain.data.drone.DroneStateType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface DroneRepository extends JpaRepository<Drone, Long> {
    Optional<Drone> findBySerialNumber(String serialNumber);

    List<Drone> findAllByState(DroneStateType state);
}
