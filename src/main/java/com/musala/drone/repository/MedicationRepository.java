package com.musala.drone.repository;

import com.musala.drone.domain.data.medication.Medication;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MedicationRepository extends JpaRepository<Medication, Long> {
    List<Medication> findAllByDroneIdAndLoadingSession(long droneId, String loadingSession);
}