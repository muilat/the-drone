package com.musala.drone.validator;

import com.musala.drone.validator.constraint.EnumValueValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

@Documented
@Constraint(validatedBy = {EnumValueValidator.class})
@Retention(RetentionPolicy.RUNTIME)

@Target({
        ANNOTATION_TYPE,
        CONSTRUCTOR, FIELD,
        METHOD, PARAMETER
})
public @interface EnumValue {
    public abstract String message() default "{validation.enum.message}";

    public abstract Class<?>[] groups() default {};

    public abstract Class<? extends Payload>[] payload() default {};

    public abstract Class<? extends java.lang.Enum<?>> enumClass();
}