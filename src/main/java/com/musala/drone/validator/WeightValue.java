package com.musala.drone.validator;

import com.musala.drone.validator.constraint.WeightValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({FIELD})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = {WeightValidator.class})
public @interface WeightValue {

    /**
     * @return value the element must be lower or equal to
     */
    float value();

    String message() default "must be grater than 0 and less or equal to {value}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};


}

