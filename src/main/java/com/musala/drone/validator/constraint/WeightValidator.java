package com.musala.drone.validator.constraint;

import com.musala.drone.validator.WeightValue;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class WeightValidator implements ConstraintValidator<WeightValue, Float> {
    private float constraintValue;

    @Override
    public void initialize(WeightValue constraintAnnotation) {
        this.constraintValue = constraintAnnotation.value();
    }

    @Override
    public boolean isValid(Float value, ConstraintValidatorContext context) {
        if (value == null) return false;

        else return value > 0 && value <= this.constraintValue;
    }
}
