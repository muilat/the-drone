package com.musala.drone.controller;

import com.musala.drone.domain.data.drone.Drone;
import com.musala.drone.domain.data.medication.Medication;
import com.musala.drone.domain.dto.drone.DroneCreateInput;
import com.musala.drone.domain.dto.medication.MedicationCreateInput;
import com.musala.drone.service.DroneService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@Validated
@RestController
@RequestMapping("/drone/api/v1/")
public class DroneController {
    private final DroneService droneService;

    @Autowired
    public DroneController(DroneService droneService) {
        this.droneService = droneService;
    }

    @Operation(summary = "Register a new drone")
    @PostMapping("")
    public ResponseEntity<Drone> registerDrone(@Valid @RequestBody DroneCreateInput droneInput) {
        log.info("Registering drone {}", droneInput.getSerialNumber());
        Drone drone = droneService.registerDrone(droneInput);
        log.info("Registered drone {}", droneInput.getSerialNumber());
        return ResponseEntity.ok(drone);
    }

    @Operation(summary = "Load a list of medications into a drone")
    @PostMapping("{droneId}/medications")
    public ResponseEntity<Drone> loadDroneWithMedications(@PathVariable(name = "droneId") long droneId,
                                                          @Valid @RequestBody List<MedicationCreateInput> medicationCreateInputs) {
        log.info("Loading drone {} with {} medications", droneId, medicationCreateInputs.size());
        Drone drone = droneService.loadDroneWithMedications(droneId, medicationCreateInputs);
        log.info("Loaded drone {} with {} medications", droneId, medicationCreateInputs.size());
        return ResponseEntity.ok(drone);
    }

    @Operation(summary = "Fetch all the (current) medications loaded into a drone")
    @GetMapping("{droneId}/medications")
    public ResponseEntity<List<Medication>> getDroneMedications(@PathVariable(name = "droneId") long droneId) {
        log.info("Fetching loaded medications for drone {}", droneId);
        List<Medication> medications = droneService.getDroneMedications(droneId);
        log.info("Fetched loaded medications for drone {}", droneId);
        return ResponseEntity.ok(medications);
    }

    @Operation(summary = "Fetch all drones in IDLE state")
    @GetMapping("available")
    public ResponseEntity<List<Drone>> getAvailableDrones() {
        log.info("Checking available drones for loading");
        List<Drone> drones = droneService.getAvailableDrones();
        log.info("Checked available drones for loading");
        return ResponseEntity.ok(drones);
    }

    @Operation(summary = "Get the battery level of a specific drone")
    @GetMapping("{droneId}/battery-level")
    public ResponseEntity<Float> getDroneBatteryLevel(@PathVariable(name = "droneId") long droneId) {
        log.info("Checking battery level for drone {}", droneId);
        Float batteryLevel = droneService.getDroneBatteryLevel(droneId);
        log.info("Checked battery level for drone {}", droneId);
        return ResponseEntity.ok(batteryLevel);
    }
}
