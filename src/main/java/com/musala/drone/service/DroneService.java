package com.musala.drone.service;

import com.musala.drone.constants.DroneConstant;
import com.musala.drone.domain.data.drone.Drone;
import com.musala.drone.domain.data.drone.DroneStateType;
import com.musala.drone.domain.data.medication.Medication;
import com.musala.drone.domain.dto.drone.DroneCreateInput;
import com.musala.drone.domain.dto.medication.MedicationCreateInput;
import com.musala.drone.mapper.DroneMapper;
import com.musala.drone.repository.DroneRepository;
import com.musala.drone.repository.MedicationRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DroneService {
    private final DroneRepository droneRepository;
    private final MedicationRepository medicationRepository;
    private final DroneMapper droneMapper;
    private final DroneConstant droneConstant;

    @Autowired
    public DroneService(DroneRepository droneRepository, MedicationRepository medicationRepository, DroneMapper droneMapper, DroneConstant droneConstant) {
        this.droneRepository = droneRepository;
        this.medicationRepository = medicationRepository;
        this.droneMapper = droneMapper;
        this.droneConstant = droneConstant;
    }

    /**
     * @param droneInput object with the properties of the drone to be registered
     * @return Drone - newly registered drone
     */
    public Drone registerDrone(DroneCreateInput droneInput) {
        droneRepository.findBySerialNumber(droneInput.getSerialNumber()).ifPresent(drone -> {
            throw new IllegalStateException(String.format("Drone with serial number %s already exists", drone.getSerialNumber()));
        });
        Drone newDrone = droneMapper.droneCreateInputToDrone(droneInput);
        newDrone.setState(DroneStateType.IDLE);
        return droneRepository.save(newDrone);
    }

    /**
     * @param droneId                unique identifier of the drone to be loaded
     * @param medicationCreateInputs list of medications to be loaded into the drone
     * @return Drone
     * @throws java.util.NoSuchElementException if a drone with the id does not exist
     */
    public Drone loadDroneWithMedications(long droneId, List<MedicationCreateInput> medicationCreateInputs) {
        Drone drone = droneRepository.findById(droneId).orElseThrow();
        if (drone.getBatteryCapacity() < droneConstant.getBatteryCapacityLowLevel()) {
            throw new IllegalStateException(String.format("The drone battery capacity is below %d%%", droneConstant.getBatteryCapacityLowLevel()));
        }
        if (!drone.getState().equals(DroneStateType.IDLE) && !drone.getState().equals(DroneStateType.LOADING)) {
            throw new IllegalStateException("The drone can not be loaded at this state");
        }
        if (drone.getState().equals(DroneStateType.IDLE)) {
            drone.setState(DroneStateType.LOADING);
            drone.setActiveLoadingSession(UUID.randomUUID().toString());
            droneRepository.save(drone);

            log.info("Updated drone state to {}", DroneStateType.LOADING);
        }

        List<Medication> droneMedications = medicationRepository.findAllByDroneIdAndLoadingSession(droneId, drone.getActiveLoadingSession());
        validateWight(drone, droneMedications, medicationCreateInputs);

        List<Medication> newMedications = medicationCreateInputs.stream().map(input -> {
            Medication medication = droneMapper.medicationCreateInputToMedication(input);
            medication.setDroneId(drone.getId());
            medication.setLoadingSession(drone.getActiveLoadingSession());
            return medication;
        }).collect(Collectors.toList());

        medicationRepository.saveAll(newMedications);

        return drone;
    }

    /**
     * @param droneId unique identifier of the drone to fetch it's loaded medication
     * @return list of medications in the drone
     * @throws java.util.NoSuchElementException if a drone with the id does not exist
     */
    public List<Medication> getDroneMedications(long droneId) {
        Drone drone = droneRepository.findById(droneId).orElseThrow();
        return medicationRepository.findAllByDroneIdAndLoadingSession(droneId, drone.getActiveLoadingSession());
    }

    private void validateWight(Drone drone, List<Medication> droneMedications, List<MedicationCreateInput> medicationCreateInputs) {
        AtomicReference<Float> medicationWeight = new AtomicReference<>((float) 0);
        AtomicReference<Float> droneCurrentWeight = new AtomicReference<>((float) 0);
        droneMedications.forEach(med -> droneCurrentWeight.updateAndGet(v -> v + med.getWeight()));
        medicationCreateInputs.forEach(med -> medicationWeight.updateAndGet(v -> v + med.getWeight()));

        if (drone.getWeight() < medicationWeight.get() + droneCurrentWeight.get()) {
            throw new IllegalArgumentException("The drone is being overloaded");
        }
    }

    public List<Drone> getAvailableDrones() {
        return droneRepository.findAllByState(DroneStateType.IDLE);
    }

    public Float getDroneBatteryLevel(long droneId) {
        return droneRepository.findById(droneId).orElseThrow().getBatteryCapacity();
    }
}
