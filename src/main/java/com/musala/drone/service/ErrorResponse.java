package com.musala.drone.service;

import com.musala.drone.exception.Error;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.SuperBuilder;

import java.util.List;

@SuperBuilder
@AllArgsConstructor
@Data
public class ErrorResponse {
    List<Error> errors;
}
