package com.musala.drone.domain.data.drone;

import lombok.Getter;

@Getter
public enum DroneModelType {
    LIGHTWEIGHT("Lightweight"),
    MIDDLEWEIGHT("Middleweight"),
    CRUISERWEIGHT("Cruiserweight"),
    HEAVYWEIGHT("Heavyweight");

    private final String code;

    DroneModelType(String code) {
        this.code = code;
    }
}
