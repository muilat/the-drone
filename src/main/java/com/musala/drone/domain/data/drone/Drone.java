package com.musala.drone.domain.data.drone;

import com.musala.drone.validator.WeightValue;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.Instant;


@SuperBuilder
@NoArgsConstructor
@Data
@Entity
@EnableJpaAuditing
public class Drone {

    @Id
    @GeneratedValue
    private long id;

    @NotBlank
    @Size(max = 100)
    @Column(unique = true)
    private String serialNumber;

    @NotNull
    private DroneModelType model;

    @WeightValue(value = 500)
    private float weight;

    @WeightValue(value = 100)
    private float batteryCapacity;

    @NotNull
    private DroneStateType state;

    @CreationTimestamp
    private Instant createdDate;

    @UpdateTimestamp
    private Instant lastModifiedDate;

    private String activeLoadingSession;

}
