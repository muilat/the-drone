package com.musala.drone.domain.data.drone;

public enum DroneStateType {
    IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING
}
