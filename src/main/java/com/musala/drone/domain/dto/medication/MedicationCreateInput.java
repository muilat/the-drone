package com.musala.drone.domain.dto.medication;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@SuperBuilder
@NoArgsConstructor
@Data
public class MedicationCreateInput {
    @NotBlank
    @Pattern(regexp = "^[a-zA-Z0-9\\-_]+$")
    private String name;

    @NotNull
    private float weight;

    @NotBlank
    @Pattern(regexp = "^[A-Z0-9_]+$")
    private String code;

    @NotBlank
    private String image;
}
