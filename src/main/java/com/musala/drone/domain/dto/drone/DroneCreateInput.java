package com.musala.drone.domain.dto.drone;

import com.musala.drone.domain.data.drone.DroneModelType;
import com.musala.drone.validator.WeightValue;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@SuperBuilder
@NoArgsConstructor
@Data
public class DroneCreateInput {
    @NotBlank
    @Size(max = 100)
    private String serialNumber;

    @NotNull
    private DroneModelType model;

    @WeightValue(value = 500)
    private float weight;

    @WeightValue(value = 100)
    private float batteryCapacity;
}
