package com.musala.drone.controller;

import com.musala.drone.domain.data.drone.Drone;
import com.musala.drone.domain.dto.drone.DroneCreateInput;
import com.musala.drone.domain.dto.medication.MedicationCreateInput;
import com.musala.drone.service.DroneService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DroneControllerTest {
    @InjectMocks
    DroneController toTest;
    @Mock
    DroneService droneService;

    @Test
    void registerDrone_withExistingSerialNumber() {
        when(droneService.registerDrone(new DroneCreateInput())).thenThrow(IllegalStateException.class);
        assertThrows(IllegalStateException.class, () -> toTest.registerDrone(new DroneCreateInput()));
    }

    @Test
    void registerDrone() {
        when(droneService.registerDrone(new DroneCreateInput())).thenReturn(new Drone());
        ResponseEntity<Drone> response = toTest.registerDrone(new DroneCreateInput());
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void loadDroneWithMedicationsThrowsIllegalStateException() {
        when(droneService.loadDroneWithMedications(anyLong(), any())).thenThrow(IllegalStateException.class);
        assertThrows(IllegalStateException.class, () -> toTest.loadDroneWithMedications(1, List.of(new MedicationCreateInput())));
    }

    @Test
    void loadDroneWithMedicationsThrowsIllegalArgumentException() {
        when(droneService.loadDroneWithMedications(anyLong(), any())).thenThrow(IllegalArgumentException.class);
        assertThrows(IllegalArgumentException.class, () -> toTest.loadDroneWithMedications(1, List.of(new MedicationCreateInput())));
    }

    @Test
    void loadDroneWithMedications() {
        when(droneService.loadDroneWithMedications(anyLong(), any())).thenReturn(new Drone());

        ResponseEntity<Drone> response = toTest.loadDroneWithMedications(1, List.of(new MedicationCreateInput()));
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

}
