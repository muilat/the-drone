package com.musala.drone.mapper;

import com.musala.drone.domain.data.drone.Drone;
import com.musala.drone.domain.data.drone.DroneModelType;
import com.musala.drone.domain.data.medication.Medication;
import com.musala.drone.domain.dto.drone.DroneCreateInput;
import com.musala.drone.domain.dto.medication.MedicationCreateInput;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class DroneMapperTest {
    @Autowired
    private DroneMapperImpl droneMapper;

    @Test
    void droneCreateInputTODrone() {
        DroneCreateInput input = DroneCreateInput.builder()
                .serialNumber("SER_NO1")
                .model(DroneModelType.MIDDLEWEIGHT)
                .weight(200)
                .batteryCapacity(80)
                .build();
        Drone drone = droneMapper.droneCreateInputToDrone(input);

        assertEquals(input.getSerialNumber(), drone.getSerialNumber());
        assertEquals(input.getModel(), drone.getModel());
        assertEquals(input.getWeight(), drone.getWeight());
        assertEquals(input.getBatteryCapacity(), drone.getBatteryCapacity());
    }

    @Test
    void medicationCreateInputToMedication() {
        MedicationCreateInput input = MedicationCreateInput.builder()
                .code("AEWR")
                .weight(45)
                .name("New Med")
                .image("base64(dfghjkl)")
                .build();
        Medication medication = droneMapper.medicationCreateInputToMedication(input);

        assertEquals(input.getCode(), medication.getCode());
        assertEquals(input.getName(), medication.getName());
        assertEquals(input.getWeight(), medication.getWeight());
        assertEquals(input.getImage(), medication.getImage());
    }
}
