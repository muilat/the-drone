package com.musala.drone.service;

import com.musala.drone.constants.DroneConstant;
import com.musala.drone.domain.data.drone.Drone;
import com.musala.drone.domain.data.drone.DroneModelType;
import com.musala.drone.domain.data.drone.DroneStateType;
import com.musala.drone.domain.data.medication.Medication;
import com.musala.drone.domain.dto.drone.DroneCreateInput;
import com.musala.drone.domain.dto.medication.MedicationCreateInput;
import com.musala.drone.mapper.DroneMapperImpl;
import com.musala.drone.repository.DroneRepository;
import com.musala.drone.repository.MedicationRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@SpringBootTest(classes = {
        DroneService.class,
        DroneMapperImpl.class
})
public class DroneServiceTest {
    @Autowired
    private DroneService toTest;
    @MockBean
    private DroneRepository droneRepository;
    @MockBean
    private MedicationRepository medicationRepository;
    @MockBean
    private DroneConstant droneConstant;
    @Autowired
    DroneMapperImpl droneMapper;

    private DroneCreateInput droneCreateInput;
    private Drone drone;
    private MedicationCreateInput medicationCreateInput;
    private Medication medication;
    private static final long DRONE_ID = 1;

    @BeforeEach
    void setUp() {
        droneCreateInput = DroneCreateInput.builder()
                .serialNumber("12wer")
                .batteryCapacity(60)
                .model(DroneModelType.MIDDLEWEIGHT)
                .weight(200)
                .build();

        medicationCreateInput = MedicationCreateInput.builder()
                .code("CDE")
                .name("Medication")
                .weight(50)
                .image("base64(dfgvbu6i8")
                .build();

        drone = droneMapper.droneCreateInputToDrone(droneCreateInput);
        drone.setId(DRONE_ID);
        drone.setState(DroneStateType.IDLE);

        medication = droneMapper.medicationCreateInputToMedication(medicationCreateInput);
        medication.setDroneId(drone.getId());
    }

    @Test
    void registerDrone_withExistingSerialNumber() {
        when(droneRepository.findBySerialNumber(anyString())).thenReturn(Optional.ofNullable(drone));

        assertThrows(IllegalStateException.class, () -> toTest.registerDrone(droneCreateInput));
    }

    @Test
    void registerDrone() {
        when(droneRepository.findBySerialNumber(anyString())).thenReturn(Optional.empty());
        when(droneRepository.save(any())).thenReturn(drone);
        Drone drone = toTest.registerDrone(droneCreateInput);

        assertNotNull(drone);
        System.out.println(drone.getId());
        assertNotEquals(0, drone.getId());
        assertEquals(DroneStateType.IDLE, drone.getState());
    }

    @Test
    void loadDroneWithMedications_invalidID() {
        when(droneRepository.findById(any())).thenReturn(Optional.empty());
        assertThrows(NoSuchElementException.class, () -> toTest.loadDroneWithMedications(DRONE_ID, List.of(medicationCreateInput)));

    }

    @Test
    void loadDroneWithMedications_lowBattery() {
        drone.setBatteryCapacity(23);
        when(droneRepository.findById(any())).thenReturn(Optional.of(drone));
        when(droneConstant.getBatteryCapacityLowLevel()).thenReturn(25);
        assertThrows(IllegalStateException.class, () -> toTest.loadDroneWithMedications(DRONE_ID, List.of(medicationCreateInput)));

    }

    @Test
    void loadDroneWithMedications_overloaded() {
        medication.setWeight(500);
        drone.setActiveLoadingSession("active");
        when(droneRepository.findById(any())).thenReturn(Optional.of(drone));
        when(medicationRepository.findAllByDroneIdAndLoadingSession(anyLong(), anyString())).thenReturn((List.of(medication)));

        assertThrows(IllegalArgumentException.class, () -> toTest.loadDroneWithMedications(DRONE_ID, List.of(medicationCreateInput)));

    }

    @Test
    void loadDroneWithMedications_notAvailable() {
        drone.setState(DroneStateType.DELIVERING);
        when(droneRepository.findById(any())).thenReturn(Optional.of(drone));
        when(medicationRepository.findAllByDroneIdAndLoadingSession(anyLong(), anyString())).thenReturn((List.of(medication)));

        assertThrows(IllegalStateException.class, () -> toTest.loadDroneWithMedications(DRONE_ID, List.of(medicationCreateInput)));

    }

    @Test
    void loadDroneWithMedications() {
        when(droneRepository.findById(any())).thenReturn(Optional.of(drone));
        when(medicationRepository.findAllByDroneIdAndLoadingSession(anyLong(), anyString())).thenReturn((List.of(medication)));

        drone = toTest.loadDroneWithMedications(DRONE_ID, List.of(medicationCreateInput));

        assertNotNull(drone);
        assertNotNull(drone.getActiveLoadingSession());

        verify(droneRepository, times(1)).save(any());
        ArgumentCaptor<List<Medication>> medicationCaptor = ArgumentCaptor.forClass(List.class);
        verify(medicationRepository, times(1)).saveAll(medicationCaptor.capture());

        assertEquals(1, medicationCaptor.getValue().size());
        assertEquals(DRONE_ID, medicationCaptor.getValue().get(0).getDroneId());
        assertNotNull(medicationCaptor.getValue().get(0).getLoadingSession());

    }

}
